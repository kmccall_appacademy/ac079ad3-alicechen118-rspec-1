def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  total = 0
  arr.each { |el| total += el }
  total
end

def multiply(*args)
  total = 1
  args.each { |num| total *= num }
  total
end

def power(num1, num2)
  num1**num2
end

def factorial(n)
  return 1 if n == 0
  return 1 if n == 1
  return n * factorial(n - 1)
end
