def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, times=2)
  repeated = []
  times.times { repeated << string }
  repeated.join(" ")
end

def start_of_word(word, num)
  word[0...num]
end

def first_word(string)
  words = string.split(" ")
  words[0]
end

LITTLE_WORDS = ["and", "the", "over"]

def titleize(title)
  words = title.split(" ")
  new_words = words.map.with_index { |word, idx|
    if idx == 0
      word.capitalize
    elsif LITTLE_WORDS.include?(word)
      word.downcase
    else
      word.capitalize
    end
  }
  new_words.join(" ")
end
