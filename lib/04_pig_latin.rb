VOWELS = %w(a e i o u A E I O U)

def translate(string)
  translated = []
  string.split.map do |word|
    translated << translate_word(word)
  end
  translated.join(" ")
end


def translate_word(word)
  word.each_char.with_index do |char, idx|
    next if !VOWELS.include?(char)
    if idx == 0
      return "#{word}ay"
    else
      if word[idx - 1] == "q"
        return word[(idx + 1)..-1] + word[0..idx] + "ay"
      else
        return word[idx..-1] + word[0...idx] + "ay"
      end
    end
  end
end
